const alturatotal = document.querySelectorAll(".altura");
const larguratotal = document.querySelectorAll(".largura");
const portastotal = document.querySelectorAll(".portas");
const janelastotal = document.querySelectorAll(".janelas");
const modalErro = document.querySelector(".modal-backdrop");
const mensagemErro = document.querySelector(".mensagem h1");
const resultado = document.querySelector(".resultado");
const textoResultado = document.querySelector(".resultado h1");
const resultadoLatas = document.querySelectorAll(".resultado h2");
const inputsContainer = document.querySelector(".inputs-container");
const bttnCalcular = document.querySelector(".bttn-calcular");
const bttnVoltar = document.querySelector(".bttn-voltar");
const rendimento1 = (0.5 * 5); //2,5m2
const rendimento2 = (2.5 * 5); // 12,5m2
const rendimento3 = (3.6 * 5); // 18m2
const rendimento4 = (18 * 5); // 90m2
const areaJanela = 2.00 * 1.20;
const areaPorta = 0.80 * 1.90;
let area = 0;
let areaTotalJanelaPorta = 0;

function obterMedidas() {
	for (let i = 0; i < 4; i++) {
		let janela = janelastotal[i].value;
		let porta = portastotal[i].value;
		let altura = alturatotal[i].value;
		let largura = larguratotal[i].value;

		if (porta != 0 && altura < 2.21) {
			return exibirModal("A altura minima da parede que contém porta não pode ser menor que 2.21m");
		}
		let parede = (largura * altura);
		let areaJanelaPorta = (areaJanela * janela) + (areaPorta * porta);
		if (parede < 1 || parede > 15) {
			return exibirModal("A area de cada parede deve ser maior que 1m² e menor que 16m²");
		}

		if (areaJanelaPorta > (parede / 2)) {
			return exibirModal("A área de portas e janelas não podem ter mais que a metade da área da parede");
		}
		areaTotalJanelaPorta += areaJanelaPorta;
		area += parede;


	}
	area -= areaTotalJanelaPorta;
	calcularLatas(area);
	limparCampos();
}

function calcularLatas(area) {
	let lata18 = 0;
	let lata36 = 0;
	let lata25 = 0;
	let lata05 = 0;

	while (area > 0) {
		if (area >= rendimento4) {
			area -= rendimento4;
			lata18++;
		}
		if (area < rendimento4 && area >= rendimento3) {
			area -= rendimento3;
			lata36++;
		}
		if (area < rendimento3 && area > rendimento2) {
			area -= rendimento2;
			lata25++;
		}
		if (area < rendimento2 && area > 0) {
			area -= rendimento1;
			lata05++;
			if (lata05 == 5) {
				lata25 += 1;
				lata05 = 0;
			}
		}
	}
	return exibirResultado(lata05, lata25, lata36, lata18);

}

function exibirModal(mensagem) {
	modalErro.classList.toggle("exibir-modal");
	mensagemErro.innerText = `${mensagem}`;
}

function limparCampos() {
	area = 0;
	areaTotalJanelaPorta = 0;
}

function exibirResultado(lata05, lata25, lata36, lata18) {
	resultado.classList.toggle("hidden");
	inputsContainer.classList.toggle("hidden");
	bttnVoltar.classList.remove("hidden");
	bttnCalcular.classList.add("hidden");
	textoResultado.innerText = `Você tem ${area.toFixed(1)}m² de área a ser pintada e precisará de: `;
	let [campo4, campo3, campo2, campo1] = resultadoLatas;

	lata18 > 0 ? campo4.innerText = `${lata18} ${lata18 > 1 ? "Latas" : "Lata"} de 18l` : "";
	lata36 > 0 ? campo3.innerText = `${lata36} ${lata36 > 1 ? "Latas" : "Lata"} de 3.6l` : "";
	lata25 > 0 ? campo2.innerText = `${lata25} ${lata25 > 1 ? "Latas" : "Lata"} de 2.5l` : "";
	lata05 > 0 ? campo1.innerText = `${lata05} ${lata05 > 1 ? "Latas" : "Lata"} de 0.5l` : "";

}

function voltar() {
	let [campo4, campo3, campo2, campo1] = resultadoLatas;
	campo1.innerText = "";
	campo2.innerText = "";
	campo3.innerText = "";
	campo4.innerText = "";
	bttnVoltar.classList.add("hidden");
	bttnCalcular.classList.remove("hidden");
	resultado.classList.toggle("hidden");
	inputsContainer.classList.toggle("hidden");
}